<?php

namespace App;

class Truck
{
    public function deliver()
    {
        echo "Truck is delivering...";
    }
}

function createDelivery()
{
    $truck = new Truck();
    $truck->deliver();
}

createDelivery();

// Résultat attendu :
/*
New delivery via truck :
Better hit the road.
Now via ship :
From sea to shining sea.
*/
