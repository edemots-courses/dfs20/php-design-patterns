<?php

namespace App;

interface StropePaymentInterface
{
    public function amountInCents();
}

class Payment
{
    private $amount;

    public function __construct($amount)
    {
        $this->amount = $amount;
    }

    public function amountInEuros() {
        return $this->amount;
    }
}

function sendPayment(StropePaymentInterface $payment)
{
    echo $payment->amountInCents();
}

$payment = new Payment(10);
echo $payment->amountInEuros();
echo PHP_EOL;
