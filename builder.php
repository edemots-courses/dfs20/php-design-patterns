<?php

namespace App;

class QueryBuilder
{
    /**
     * Attribut "tampon" construit pas à pas qui va contenir les étapes de notre requete
     */
    protected $query;

    /**
     * Mets notre attribut tampon à zéro
     */
    protected function reset()
    {
        // Instance d'un objet vide
        $this->query = new \stdClass();
    }

    public function __construct($select, $where, $limit = null)
    {
        // Reset
        $this->reset();

        // Construire la base du select
        // $select = [
        //      'fields' => ['*'],
        //      'table' => 'user'
        // ]
        $this->query->base = 'SELECT '.implode(', ', $select['fields']).' FROM '
            .$select['table'];
        // => $this->query->base = 'SELECT * FROM user'

        foreach ($where as $field => $value) {
            // $where = [
            //      'username' => [
            //          'operator' => '=',
            //          'value" => 'test'
            //      ],
            //      'status' => [
            //          'operator' => '>',
            //          'value" => 2
            //      ],
            // ]
            $this->query->where[] = "$field ".$value['operator']." '"
                .$value['value']."'";
        }
        // => $this->query->where = [
        //      "username = 'test'",
        //      "status > '2'"
        // ]

        if ($limit !== null) {
            $this->query->limit = ' LIMIT '.$limit['limit'].' OFFSET '
                .$limit['offset'];
            // => $this->query->limit = ' LIMIT 1 OFFSET 0'
        }

    }

    public function getSQL()
    {
        $sql = $this->query->base;
        // $sql = 'SELECT * FROM user'
        if (!empty($this->query->where)) {
            $sql .= " WHERE ".implode(" AND ", $this->query->where);
            // $sql = 'SELECT * FROM user WHERE username = 'test' AND status > '2''
        }
        if ($this->query->limit !== null) {
            $sql .= $this->query->limit;
            // $sql = 'SELECT * FROM user WHERE username = 'test' AND status > '2' LIMIT 1 OFFSET 0'
        }
        $sql .= ";";
        // $sql = 'SELECT * FROM user WHERE username = 'test' AND status > '2' LIMIT 1 OFFSET 0;'

        return $sql;
    }
}

function getUser()
{
    echo (new QueryBuilder(
            ['table' => 'users', 'fields' => ['*']],
            ['username' => ['operator' => '=', 'value' => 'test'], 'status' => ['operator' => '>', 'value' => 2]],
            ['limit' => 1, 'offset' => 0])
        )->getSQL().PHP_EOL;
}

getUser();

// Résultat attendu :
/*
SELECT * FROM users WHERE username = 'test' LIMIT 1 OFFSET 0;
*/
