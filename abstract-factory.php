<?php

namespace App;

class NeapolitanMargherita
{
    public function eat()
    {
        echo 'Sei bella come una margherita napoletana!'.PHP_EOL;
    }

    public function cook()
    {
        echo 'Croccante napoletana margherita!'.PHP_EOL;
    }

    public function flip()
    {
        echo 'Un salto di margherita napoletana!'.PHP_EOL;
    }
}

function createPizza()
{
    $margherita = new NeapolitanMargherita();

    $margherita->flip();
    $margherita->cook();
    $margherita->eat();
}

createPizza();

// Résultat attendu :
/*
Un salto di margherita napoletana!
Croccante napoletana margherita!
Sei bella come una margherita napoletana!

Un salto di regina napoletana!
Croccante napoletana regina!
Sei bella come una regina napoletana!

--
Un salto di margherita romana!
Croccante romana margherita!
Sei bella come una margherita romana!

Un salto di regina romana!
Croccante romana regina!
Sei bella come una regina romana!

--
Un salto di margherita veneziana!
Croccante veneziana margherita!
Sei bella come una margherita veneziana!

Un salto di regina veneziana!
Croccante veneziana regina!
Sei bella come una regina veneziana!
*/
