<?php

namespace App;

class Server
{
    private $users = [];

    public function registerUser($email, $password, $role = "ROLE_USER")
    {
        $this->users[$email] = [
            'password' => $password,
            'role' => $role
        ];
    }

    public function userExists($email)
    {
        return isset($this->users[$email]);
    }

    public function isPasswordValid($email, $password)
    {
        return $this->users[$email]['password'] === $password;
    }

    public function userHasRole($email, $role)
    {
        return $this->users[$email]['role'] === $role;
    }
}

class Router
{
    public function handleRoute($route)
    {
        echo $route.PHP_EOL;
    }
}

function callRouter(Server $server, $email, $password, $role = "ROLE_USER")
{
    $router = new Router();
    if ($server->userExists($email) && $server->isPasswordValid($email, $password) && $server->userHasRole($email, $role)) {
        $router->handleRoute('/admin');

        return;
    }

    echo "Erreur".PHP_EOL;
}

$server = new Server();
$server->registerUser('toto@admin.fr', 'toto', 'ROLE_ADMIN');
$server->registerUser('tata@truc.com', 'titi93');

callRouter($server, 'toto@admin.fr', 'unmotdepasse', "ROLE_ADMIN");
callRouter($server, 'tata@truc.com', 'titi93', "ROLE_ADMIN");
callRouter($server, 'toto@admin.fr', 'toto', "ROLE_ADMIN");

// Résultat attendu
/*
UserExistsMiddleware: Le mot de passe est incorrect !
--
CheckRoleMiddleware: L'utilisateur n'a pas le role requis !
--
CheckRoleMiddleware: Hello, toto@admin.fr
Router: Successful authentication to : /admin
*/
