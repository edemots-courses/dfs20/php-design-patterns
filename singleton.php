<?php

namespace App;

class Database
{
    private static $connexion = null;

    public static function getInstance()
    {
        if (self::$connexion === null) {
            // new PDO('pgsql:host=localhost;port=5432', 'test', 'test);
            self::$connexion = ['pgsql:host=localhost;port=5432', 'test', 'test'];
        }

        return self::$connexion;
    }
}

function test()
{
    var_dump(Database::getInstance());
}

function test2()
{
    var_dump(Database::getInstance());
}

test();
test2();
