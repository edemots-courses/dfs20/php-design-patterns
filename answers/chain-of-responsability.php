<?php

namespace App\Answers;

class Server
{
    private $users = [];

    public function registerUser($email, $password, $role = "ROLE_USER")
    {
        $this->users[$email] = [
            'password' => $password,
            'role' => $role
        ];
    }

    public function userExists($email)
    {
        return isset($this->users[$email]);
    }

    public function isPasswordValid($email, $password)
    {
        return $this->users[$email]['password'] === $password;
    }

    public function userHasRole($email, $role)
    {
        return $this->users[$email]['role'] === $role;
    }
}

abstract class Middleware
{
    /**
     * @var Middleware
     */
    protected $next;

    /**
     * @var Server
     */
    protected $server;

    public function __construct(Server $server)
    {
        $this->server = $server;
    }

    /**
     * Permet de créer une chaîne de middlewares
     */
    public function linkWith(Middleware $next)
    {
        $this->next = $next;

        return $next;
    }

    public function handle($email, $password, $role)
    {
        // null == false
        // false == 0
        if (!$this->next) {
            return true;
        }

        return $this->next->handle($email, $password, $role);
    }
}

class UserExistsMiddleware extends Middleware
{
    public function handle($email, $password, $role)
    {
        if (!$this->server->userExists($email)) {
            echo __CLASS__.": L'utilisateur n'éxiste pas !".PHP_EOL;

            return false;
        }

        return parent::handle($email, $password, $role);
    }
}

class IsPasswordValidMiddleware extends Middleware
{
    public function handle($email, $password, $role)
    {
        if (!$this->server->isPasswordValid($email, $password)) {
            echo __CLASS__.": Le mot de passe est incorrect !".PHP_EOL;

            return false;
        }

        return parent::handle($email, $password, $role);
    }
}

class CheckUserRoleMiddleware extends Middleware
{
    public function handle($email, $password, $role)
    {
        if (!$this->server->userHasRole($email, $role)) {
            echo __CLASS__.": L'utilisateur n'a pas le rôle requis !".PHP_EOL;

            return false;
        }

        echo "Hello, $email".PHP_EOL;

        return parent::handle($email, $password, $role);
    }
}

class Router
{
    /**
     * @var Middleware
     */
    private $middleware;

    public function setMiddleware(Middleware $middleware)
    {
        $this->middleware = $middleware;
    }

    public function handleRoute($route, $email, $password, $role)
    {
        if ($this->middleware->handle($email, $password, $role)) {
            echo "Authentifié avec succès ! ".$route.PHP_EOL;

            return true;
        }

        return false;
    }
}

function callRouter(Server $server, $email, $password, $role = "ROLE_USER")
{
    $router = new Router();

    $middleware = new UserExistsMiddleware($server);
    $middleware
        ->linkWith(new IsPasswordValidMiddleware($server))
        ->linkWith(new CheckUserRoleMiddleware($server));

    $router->setMiddleware($middleware);
    $router->handleRoute("/admin", $email, $password, $role);
}

$server = new Server();
$server->registerUser('toto@admin.fr', 'toto', 'ROLE_ADMIN');
$server->registerUser('tata@truc.com', 'titi93');

callRouter($server, 'toto@admin.fr', 'unmotdepasse', "ROLE_ADMIN");
callRouter($server, 'tata@truc.com', 'titi93', "ROLE_ADMIN");
callRouter($server, 'toto@admin.fr', 'toto', "ROLE_ADMIN");

// Résultat attendu
/*
UserExistsMiddleware: Le mot de passe est incorrect !
--
CheckRoleMiddleware: L'utilisateur n'a pas le role requis !
--
CheckRoleMiddleware: Hello, toto@admin.fr
Router: Successful authentication to : /admin
*/
