<?php

namespace App\Answers;

interface Transport
{
    public function deliver();
}

class Truck implements Transport
{
    public function deliver()
    {
        echo "Better hit the road.".PHP_EOL;
    }
}

class Ship implements Transport
{
    public function deliver()
    {
        echo "From sea to shining sea.".PHP_EOL;
    }
}

function createDelivery(Transport $transport)
{
    $transport->deliver();
}

echo "New delivery via truck :".PHP_EOL;
createDelivery(new Truck());

echo "Now via ship :".PHP_EOL;
createDelivery(new Ship());
