<?php

namespace App\Answers;

interface Pizza
{
    public function eat();
    public function cook();
    public function flip();
}

class NeapolitanMargherita implements Pizza
{
    public function eat()
    {
        echo 'Sei bella come una margherita napoletana!'.PHP_EOL;
    }

    public function cook()
    {
        echo 'Croccante napoletana margherita!'.PHP_EOL;
    }

    public function flip()
    {
        echo 'Un salto di margherita napoletana!'.PHP_EOL;
    }
}

class RomanMargherita implements Pizza
{
    public function eat()
    {
        echo 'Sei bella come una margherita romana!'.PHP_EOL;
    }

    public function cook()
    {
        echo 'Croccante romana margherita!'.PHP_EOL;
    }

    public function flip()
    {
        echo 'Un salto di margherita romana!'.PHP_EOL;
    }
}

class VenetianMargherita implements Pizza
{
    public function eat()
    {
        echo 'Sei bella come una margherita veneziana!'.PHP_EOL;
    }

    public function cook()
    {
        echo 'Croccante veneziana margherita!'.PHP_EOL;
    }

    public function flip()
    {
        echo 'Un salto di margherita veneziana!'.PHP_EOL;
    }
}

class NeapolitanRegina implements Pizza
{
    public function eat()
    {
        echo 'Sei bella come una regina napoletana!'.PHP_EOL;
    }

    public function cook()
    {
        echo 'Croccante napoletana regina!'.PHP_EOL;
    }

    public function flip()
    {
        echo 'Un salto di regina napoletana!'.PHP_EOL;
    }
}

class RomanRegina implements Pizza
{
    public function eat()
    {
        echo 'Sei bella come una regina romana!'.PHP_EOL;
    }

    public function cook()
    {
        echo 'Croccante romana regina!'.PHP_EOL;
    }

    public function flip()
    {
        echo 'Un salto di regina romana!'.PHP_EOL;
    }
}

class VenetianRegina implements Pizza
{
    public function eat()
    {
        echo 'Sei bella come una regina veneziana!'.PHP_EOL;
    }

    public function cook()
    {
        echo 'Croccante veneziana regina!'.PHP_EOL;
    }

    public function flip()
    {
        echo 'Un salto di regina veneziana!'.PHP_EOL;
    }
}

interface PizzaFactory
{
    public function createMargherita(): Pizza;
    public function createRegina(): Pizza;
}

class NeapolitanPizzaFactory implements PizzaFactory
{
    public function createMargherita(): Pizza
    {
        return new NeapolitanMargherita();
    }

    public function createRegina(): Pizza
    {
        return new NeapolitanRegina();
    }
}

class RomanPizzaFactory implements PizzaFactory
{
    public function createMargherita(): Pizza
    {
        return new RomanMargherita();
    }

    public function createRegina(): Pizza
    {
        return new RomanRegina();
    }
}

class VenetianPizzaFactory implements PizzaFactory
{
    public function createMargherita(): Pizza
    {
        return new VenetianMargherita();
    }

    public function createRegina(): Pizza
    {
        return new VenetianRegina();
    }
}

function createPizza(PizzaFactory $pizzaFactory)
{
    $margherita = $pizzaFactory->createMargherita();
    $regina = $pizzaFactory->createRegina();

    $margherita->flip();
    $margherita->cook();
    $margherita->eat();
    echo PHP_EOL;

    $regina->flip();
    $regina->cook();
    $regina->eat();
    echo PHP_EOL;
}

createPizza(new NeapolitanPizzaFactory());
echo "--".PHP_EOL;
createPizza(new RomanPizzaFactory());
echo "--".PHP_EOL;
createPizza(new VenetianPizzaFactory());
