<?php

namespace App\Answers;

interface StropePaymentInterface
{
    public function amountInCents();
}

class PaymentAdapter implements StropePaymentInterface
{
    private $payment;

    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    public function amountInCents()
    {
        return $this->payment->amountInEuros() * 100;
    }
}

class Payment
{
    private $amount;

    public function __construct($amount)
    {
        $this->amount = $amount;
    }

    public function amountInEuros() {
        return $this->amount;
    }
}

function sendPayment(StropePaymentInterface $payment)
{
    echo $payment->amountInCents();
}

$payment = new Payment(10);
sendPayment(new PaymentAdapter($payment));
echo PHP_EOL;
