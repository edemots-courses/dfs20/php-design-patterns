<?php

namespace App\Answers;

class QueryBuilder
{
    /**
     * Attribut "tampon" construit pas à pas qui va contenir les étapes de notre requete
     */
    protected $query;

    /**
     * Mets notre attribut tampon à zéro
     */
    protected function reset()
    {
        // Instance d'un objet vide
        $this->query = new \stdClass();
    }

    public function select($table, $fields = ["*"])
    {
        // Reset
        $this->reset();

        // Construire la base du select
        // $select = [
        //      'fields' => ['*'],
        //      'table' => 'user'
        // ]
        $this->query->base = "SELECT ".implode(', ', $fields)." FROM $table";
        // => $this->query->base = 'SELECT * FROM user'

        return $this;
    }

    public function where($field, $value, $operator = "=")
    {
        // $where = [
        //      'username' => [
        //          'operator' => '=',
        //          'value" => 'test'
        //      ],
        //      'status' => [
        //          'operator' => '>',
        //          'value" => 2
        //      ],
        // ]
        $this->query->where[] = "$field $operator '$value'";
        // => $this->query->where = [
        //      "username = 'test'",
        //      "status > '2'"
        // ]

        return $this;
    }

    public function limit($limit = 1, $offset = 0)
    {
        $this->query->limit = " LIMIT $limit OFFSET $offset";
        // => $this->query->limit = ' LIMIT 1 OFFSET 0'

        return $this;
    }

    public function getSQL()
    {
        $sql = $this->query->base;
        // $sql = 'SELECT * FROM user'
        if (!empty($this->query->where)) {
            $sql .= " WHERE ".implode(" AND ", $this->query->where);
            // $sql = 'SELECT * FROM user WHERE username = 'test' AND status > '2''
        }
        if ($this->query->limit !== null) {
            $sql .= $this->query->limit;
            // $sql = 'SELECT * FROM user WHERE username = 'test' AND status > '2' LIMIT 1 OFFSET 0'
        }
        $sql .= ";";
        // $sql = 'SELECT * FROM user WHERE username = 'test' AND status > '2' LIMIT 1 OFFSET 0;'

        return $sql;
    }
}

function getUser(QueryBuilder $queryBuilder)
{
    echo $queryBuilder
            ->select('users', ['id', 'username', 'password'])
            ->where('username', 'test')
            ->where('status', 2, '>')
            ->limit()
            ->getSQL()
        .PHP_EOL;
}

getUser(new QueryBuilder());

// Résultat attendu :
/*
SELECT * FROM users WHERE username = 'test' LIMIT 1 OFFSET 0;
*/
