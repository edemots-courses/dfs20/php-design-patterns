<?php

namespace App\Answers;

class Database
{
    private static $connexion = null;

    public static function getInstance(): array
    {
        if (self::$connexion === null) {
            self::$connexion = ['pgsql:host=localhost;port=5432', 'test', 'test'];
        }

        return self::$connexion;
    }
}

echo Database::getInstance();
